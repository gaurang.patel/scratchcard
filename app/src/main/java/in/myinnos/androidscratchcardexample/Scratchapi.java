package in.myinnos.androidscratchcardexample;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;

public class Scratchapi {

    public static final String BASE_URL = "https://qa-api.quberapp.com";

    @SerializedName("winner")
    @Expose
    private String winner;

    @SerializedName("amount")
    @Expose
    private int amount;

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public Scratchapi(String winner, int amount, String message) {
        this.winner = winner;
        this.amount = amount;
        this.message = message;
    }

    @Override
    public String toString() {
        return "Scratchapi{" +
                "winner='" + winner + '\'' +
                ", amount=" + amount +
                ", message='" + message + '\'' +
                '}';
    }
}
