package in.myinnos.androidscratchcardexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import retrofit2.Callback;
import java.util.List;
import in.myinnos.androidscratchcard.ScratchCard;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    ListView listview;
    private TextView textView, txReload;
    private ScratchCard mScratchCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        txReload = (TextView) findViewById(R.id.txReload);

        //loadRandomNumber();
       /* Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(ScratchAPI.BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create());*/

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Scratchapi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Scratchapi api=retrofit.create(Scratchapi.class);

        Call<List<Scratch> call = api.getScratchapis();

        mScratchCard = (ScratchCard) findViewById(R.id.scratchCard);
        mScratchCard.setOnScratchListener(new ScratchCard.OnScratchListener() {
            @Override
            public void onScratch(ScratchCard scratchCard, float visiblePercent) {
                if (visiblePercent > 0.3) {
                    mScratchCard.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "Content Visible", Toast.LENGTH_SHORT).show();
                }
            }
        });

       /* txReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadRandomNumber();
            }
        });*/
    }

   /* private void loadRandomNumber(){
        Random rand = new Random();
        String randomNum = String.valueOf(20 + rand.nextInt((100 - 20) + 1));
        textView.setText(randomNum);
    }*/
}
