package in.myinnos.androidscratchcardexample;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface Scratch {

    String BASE_URL="https://qa-api.quberapp.com/";

  //  @Headers("Content-Type:application/json")

    @retrofit2.http.Headers({
            "Content-Type: application/json"
    })

    @GET("quber-service/api/v1.0/scratch-ticket")
    //  @GET(".json")

    Call<Scratchapi> getScratchapis();
}

